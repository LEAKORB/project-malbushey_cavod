﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public enum Gender
    {
        זכר,
        נקבה
    }
    public class CustomerClientVM
    {
        public List<children> listChlidren { get; set; }
        public List<size> listSize { get; set; }
        public Gender ChildGender { get; set; }
    }
}