//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class sale
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sale()
        {
            this.item_in_sale = new HashSet<item_in_sale>();
        }
    
        public int id_sale { get; set; }
        public string name_sale { get; set; }
        public int idcity_sale { get; set; }
        public int idneighborhood_sale { get; set; }
        public System.DateTime fod_sale { get; set; }
        public System.DateTime lod_sale { get; set; }
        public System.DateTime date_sale { get; set; }
        public Nullable<int> numitemssetting_sale { get; set; }
        public string addressSale { get; set; }
    
        public virtual city city { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<item_in_sale> item_in_sale { get; set; }
        public virtual neighborhood neighborhood { get; set; }
        public virtual setting setting { get; set; }
    }
}
