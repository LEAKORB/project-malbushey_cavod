﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Models
{
    public class checkExiest : ValidationAttribute
    { //בדיקה מותאמת אישית האם המודל כבר קיים 
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                var res = DBShopinCart.lShopingCard.Where(x => x.name_item == value.ToString()).ToList();
                if (res.Count == 0)
                    return ValidationResult.Success;
                else //הודאת שגיאה כאשר המודל קיים
                    return new ValidationResult("this model exiest");
            }
            else
                return ValidationResult.Success;
        }
    }
}