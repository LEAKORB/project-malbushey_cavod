//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class size
    {
        public int id_size { get; set; }
        public Nullable<int> id_item { get; set; }
        public Nullable<int> n_size { get; set; }
        public Nullable<int> amount { get; set; }
    
        public virtual item item { get; set; }
        public virtual item_in_sale item_in_sale { get; set; }
    }
}
