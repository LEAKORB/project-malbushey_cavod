﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Project.Models
{
    public class MyShoppingCart:item
    {
        public int Amount { get; set; }
        public int id_item { get; set; }
        public string name_item { get; set; }
        public int idtype_item { get; set; }
        public string picture_item { get; set; }
        public string sizeinstock_item { get; set; }
        public decimal price_item { get; set; }
        public string color_item { get; set; }
        public string limitedamount_item { get; set; }
        public int itemInWait { get; set; }
    }
}