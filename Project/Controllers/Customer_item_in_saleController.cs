﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Customer_item_in_saleController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: item_in_sale
        public ActionResult Index()
        {
            var item_in_sale = db.item_in_sale.Include(i => i.city).Include(i => i.sale);
            return View(item_in_sale.ToList());
        }

        // GET: item_in_sale/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item_in_sale item_in_sale = db.item_in_sale.Find(id);
            if (item_in_sale == null)
            {
                return HttpNotFound();
            }
            return View(item_in_sale);
        }

        // GET: item_in_sale/Create
        public ActionResult Create()
        {
            ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city");
            ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale");
            return View();
        }

        // POST: item_in_sale/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_itemsale,itsale_itemsale,iditem_itemsale,size_itemsale,amountinstock_itemsale")] item_in_sale item_in_sale)
        {
            if (ModelState.IsValid)
            {
                db.item_in_sale.Add(item_in_sale);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city", item_in_sale.itsale_itemsale);
            ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale", item_in_sale.iditem_itemsale);
            return View(item_in_sale);
        }

        // GET: item_in_sale/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item_in_sale item_in_sale = db.item_in_sale.Find(id);
            if (item_in_sale == null)
            {
                return HttpNotFound();
            }
            ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city", item_in_sale.itsale_itemsale);
            ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale", item_in_sale.iditem_itemsale);
            return View(item_in_sale);
        }

        // POST: item_in_sale/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_itemsale,itsale_itemsale,iditem_itemsale,size_itemsale,amountinstock_itemsale")] item_in_sale item_in_sale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item_in_sale).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.itsale_itemsale = new SelectList(db.city, "id_city", "name_city", item_in_sale.itsale_itemsale);
            ViewBag.iditem_itemsale = new SelectList(db.sale, "id_sale", "name_sale", item_in_sale.iditem_itemsale);
            return View(item_in_sale);
        }

        // GET: item_in_sale/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item_in_sale item_in_sale = db.item_in_sale.Find(id);
            if (item_in_sale == null)
            {
                return HttpNotFound();
            }
            return View(item_in_sale);
        }

        // POST: item_in_sale/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            item_in_sale item_in_sale = db.item_in_sale.Find(id);
            db.item_in_sale.Remove(item_in_sale);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
