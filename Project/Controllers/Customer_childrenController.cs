﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Customer_childrenController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Customer_children
        public ActionResult Index()
        {
            var children = db.children.Include(c => c.client);
            return View(children.ToList());
        }

        // GET: Customer_children/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            children child = db.children.Find(id);
            if (child == null)
            {
                return HttpNotFound();
            }
            return View(child);
        }

        //// GET: Customer_children/Create
        //public PartialViewResult Create()/*int? nc*/
        //{
        //    //var tzclient = Session["tz"];
        //    var mf = new List<string> { "זכר", "נקבה" };
        //    ViewBag.m_f_child = new SelectList(mf);
        //    return PartialView();

        //}

        // POST: Customer_children/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerClientVM children)
        {
            try
            {
                int nFile = 0;
                if (ModelState.IsValid)
                {
                    foreach (var item in children.listChlidren)
                    {
                        children child = new children();
                        child.idclient_child = item.idclient_child;
                        child.birthdate_child = item.birthdate_child;
                        if (item.m_f_child == null)
                            child.m_f_child = "זכר";
                        else
                            child.m_f_child = item.m_f_child;

                        db.children.Add(child);
                        db.SaveChanges();
                    }


                    if (Session["nameC"] != null)
                        ViewBag.name = Session["nameC"].ToString();
                    var tz = Session["tzC"];
                    client c = db.client.FirstOrDefault(a => a.tz_client == tz);
                    if (c != null)
                        nFile = db.tblfiles.Count(a => a.id_c == c.id_client);
                    if (nFile == 0)
                        return View("Upload_files");
                    var t = db.tblfiles.Where(a => a.id_c == c.id_client);
                    return View("~/Views/CustomerClients/IndexFile.cshtml", t.ToList());
                }
            }
            catch
            {

            }
            //ViewBag.idclient_child = new SelectList(db.client, "id_client", "address_client", child.idclient_child);
            return View(children);
        }

        // GET: Customer_children/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            children child = db.children.Find(id);
            if (child == null)
            {
                return HttpNotFound();
            }
            ViewBag.idclient_child = new SelectList(db.client, "id_client", "address_client", child.idclient_child);
            return View(child);
        }

        // POST: Customer_children/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_child,idclient_child,birthdate_child,m_f_child")] children child)
        {
            if (ModelState.IsValid)
            {
                db.Entry(child).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idclient_child = new SelectList(db.client, "id_client", "address_client", child.idclient_child);
            return View(child);
        }

        // GET: Customer_children/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            children child = db.children.Find(id);
            if (child == null)
            {
                return HttpNotFound();
            }
            return View(child);
        }

        // POST: Customer_children/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            children child = db.children.Find(id);
            db.children.Remove(child);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
