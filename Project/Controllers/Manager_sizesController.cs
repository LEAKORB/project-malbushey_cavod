﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_sizesController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_sizes/Create
        public ActionResult Create()
        {
            ViewBag.id_item = new SelectList(db.item, "id_item", "name_item");
            return View();
        }

        // POST: Manager_sizes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerClientVM size)
        {
            if (ModelState.IsValid)//הוספת כמה מידות לאותו מוצר ביחד
            {
                foreach (var item in size.listSize)
                {
                    if (item.amount == null)
                    {
                        ViewBag.error1 = "נא מלא את כל השדות";
                        return View("Create", size);
                    }
                    if (item.amount == 0)
                    {
                        ViewBag.error1 = "נא מלא כמות גדולה מ-0";
                        return View("Create", size);
                    }
                }
                foreach (var item in size.listSize)
                { 
                    db.size.Add(item);
                }
                db.SaveChanges();
                return RedirectToAction("Index", "Manager_item_in_sale");
            }
            return View(size);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
