﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_order_detailsController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_order_details
        public ActionResult Index()
        {
            var order_details = db.order_details.Include(o => o.item).Include(o => o.orders);
            return View(order_details.ToList());
        }

     

        // GET: Manager_order_details/Create
        public ActionResult Create()
        {
            ViewBag.iditem_details = new SelectList(db.item, "id_item", "name_item");
            ViewBag.idorder_details = new SelectList(db.orders, "id_order", "id_order");
            return View();
        }

        // POST: Manager_order_details/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idorder_details,iditem_details,size_details,amount_details,price")] order_details order_details)
        {
            try
            {
                order_details.price = (float) (order_details.amount_details * order_details.item.price_item);
                if (ModelState.IsValid)
                {
                    db.order_details.Add(order_details);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.iditem_details = new SelectList(db.item, "id_item", "name_item", order_details.iditem_details);
                ViewBag.idorder_details = new SelectList(db.orders, "id_order", "id_order", order_details.idorder_details);
                return View(order_details);
            }

            catch
            {
                ViewBag.error = "ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_order_details/Edit/5
        public ActionResult Edit(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_details order_details = db.order_details.Find(id);
            if (order_details == null)
            {
                return HttpNotFound();
            }
            ViewBag.iditem_details = new SelectList(db.item, "id_item", "name_item", order_details.iditem_details);
            ViewBag.idorder_details = new SelectList(db.orders, "id_order", "id_order", order_details.idorder_details);
            return View(order_details);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // POST: Manager_order_details/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idorder_details,iditem_details,size_details,amount_details,price")] order_details order_details)
        {
            try{
                order_details.price = (float)(order_details.amount_details * order_details.item.price_item);
                if (ModelState.IsValid)
            {
                db.Entry(order_details).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.iditem_details = new SelectList(db.item, "id_item", "name_item", order_details.iditem_details);
            ViewBag.idorder_details = new SelectList(db.orders, "id_order", "id_order", order_details.idorder_details);
            return View(order_details);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: Manager_order_details/Delete/5
        public ActionResult Delete(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            order_details order_details = db.order_details.Find(id);
            if (order_details == null)
            {
                return HttpNotFound();
            }
            return View(order_details);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_order_details/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{
                order_details order_details = db.order_details.Find(id);

            db.order_details.Remove(order_details);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
