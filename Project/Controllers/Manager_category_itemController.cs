﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_category_itemController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_category_item
        public ActionResult Index()
        {
            var category_item = db.category_item.Include(c => c.type_item);
            return View(category_item.ToList());
        }

      

        // GET: Manager_category_item/Create
        public ActionResult Create()
        {
            ViewBag.id_typeItem = new SelectList(db.type_item, "id_type", "name_type");
            return View();
        }

        // POST: Manager_category_item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_category,name_category,id_typeItem")] category_item category_item)
        {
            try
            { 
                if (ModelState.IsValid)
                {
                    db.category_item.Add(category_item);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.id_typeItem = new SelectList(db.type_item, "id_type", "name_type", category_item.id_typeItem);
                return View(category_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_category_item/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            category_item category_item = db.category_item.Find(id);
            if (category_item == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_typeItem = new SelectList(db.type_item, "id_type", "name_type", category_item.id_typeItem);
            return View(category_item);
        }

        // POST: Manager_category_item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_category,name_category,id_typeItem")] category_item category_item)
        {
          
                try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(category_item).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.id_typeItem = new SelectList(db.type_item, "id_type", "name_type", category_item.id_typeItem);
                return View(category_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: Manager_category_item/Delete/5
        public ActionResult Delete(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            category_item category_item = db.category_item.Find(id);
            if (category_item == null)
            {
                return HttpNotFound();
            }
            return View(category_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_category_item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{
                category_item category_item = db.category_item.Find(id);
                db.item_in_sale.RemoveRange(db.item_in_sale.Where(c => c.item.id_i_category == id));
                db.order_details.RemoveRange(db.order_details.Where(c => c.item.id_i_category == id));
                db.item.RemoveRange(db.item.Where(c => c.id_i_category == id));
                db.category_item.Remove(category_item);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים"; 
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
