﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{

    public class Manager_childrenController : Controller
    {
        private P2Entities2 db = new P2Entities2();
        // GET: Manager_children
        public ActionResult Index()
        {
            var children = db.children.Include(c => c.client);
            return View(children.ToList());
        }


        // GET: Manager_children/Create
        public ActionResult Create()
        {
            CustomerClientVM ccVM = new CustomerClientVM();
            return View(ccVM);
        }

        // POST: Manager_children/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerClientVM children, string tzt)
        {
            try
            {
                //הוספת ילדים לרשום 
                if (ModelState.IsValid)
                {
                    client c = db.client.First(t => t.tz_client == tzt);
                    if (c != null)
                    {
                        children child = new children();
                        child.idclient_child = c.id_client;
                        child.birthdate_child = children.listChlidren[0].birthdate_child;
                        if (children.listChlidren[0].m_f_child == null)
                            child.m_f_child = "זכר";
                        else
                            child.m_f_child = children.listChlidren[0].m_f_child;
                        db.children.Add(child);
                        c.numchildren_client = c.numchildren_client + 1;
                        db.Entry(c).State = EntityState.Modified;
                        db.SaveChanges();
                       
                    }
                    else
                    {
                        ViewBag.error = ".משתמש אינו קיים";
                        return View(children);
                    }
                }
            } 
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View(children);
            }
            return RedirectToAction("Index");
        }

        // GET: Manager_children/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            children children = db.children.Find(id);
            if (children == null)
            {
                return HttpNotFound();
            }
            ViewBag.idclient_child = new SelectList(db.client, "id_client", "address_client", children.idclient_child);
            return View(children);
        }

        // POST: Manager_children/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_child,idclient_child,birthdate_child,m_f_child")] children children)
        {
            if (ModelState.IsValid)
            {
                db.Entry(children).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idclient_child = new SelectList(db.client, "id_client", "address_client", children.idclient_child);
            return View(children);
        }

        // GET: Manager_children/Delete/5
        public ActionResult Delete(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            children children = db.children.Find(id);
            if (children == null)
            {
                return HttpNotFound();
            }
            return View(children);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_children/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{
                children children = db.children.Find(id);
                client c = db.client.First(t => t.id_client == children.idclient_child);
                db.children.Remove(children);
                c.numchildren_client = c.numchildren_client - 1;
                db.Entry(c).State = EntityState.Modified;
                db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }
        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {

            var res = db.children.Where(a => a.client.nameclient.Contains(searchTxt) || a.client.tz_client.Contains(searchTxt)
            || a.m_f_child.Contains(searchTxt)).ToList();
            if (res.Count() != 0)
            {
                return PartialView(res);
            }
            else
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                return PartialView(res);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
