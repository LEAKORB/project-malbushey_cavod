﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_usersController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_users
        public ActionResult Index()
        {
            return View(db.users.ToList());
        }

        // GET: Manager_users/Create
        public ActionResult Create()
        {           
            return View();
        }

        // POST: Manager_users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_user,name_user,password_user")] users users)
        {
            try
            {
                users.password_user = "";
                if (ModelState.IsValid)
                {
                    db.users.Add(users);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(users);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_users/Edit/5
        public ActionResult Edit(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            users users = db.users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // POST: Manager_users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_user,name_user,password_user")] users users)
        {
           try{
                if (ModelState.IsValid)
            {
                    users.password_user = "";
                    db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(users);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: Manager_users/Delete/5
        public ActionResult Delete(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            users users = db.users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{
                //מחיקת כל הנתונים המקושרים ליוזר זה
                users users = db.users.Find(id);
                db.item_in_wait.RemoveRange(db.item_in_wait.Where(c => c.client.users.id_user == id));
                db.order_details.RemoveRange(db.order_details.Where(c => c.orders.client.users.id_user == id));
                db.orders.RemoveRange(db.orders.Where(c => c.client.users.id_user == id));
                db.tblfiles.RemoveRange(db.tblfiles.Where(c => c.client.users.id_user == id));
                db.children.RemoveRange(db.children.Where(c => c.client.users.id_user == id));
                db.client.RemoveRange(db.client.Where(c => c.users.id_user == id));
                db.users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
