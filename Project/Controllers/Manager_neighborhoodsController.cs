﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_neighborhoodsController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_neighborhoods
        public ActionResult Index()
        {
            var neighborhood = db.neighborhood.Include(n => n.city);
            return View(neighborhood.ToList());
        }

     

        // GET: Manager_neighborhoods/Create
        public ActionResult Create()
        {
            ViewBag.idcity_neighborhood = new SelectList(db.city, "id_city", "name_city");
            return View();
        }

        // POST: Manager_neighborhoods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_neighborhood,name_neighborhood,idcity_neighborhood")] neighborhood neighborhood)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.neighborhood.Add(neighborhood);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.idcity_neighborhood = new SelectList(db.city, "id_city", "name_city", neighborhood.idcity_neighborhood);
                return View(neighborhood);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_neighborhoods/Edit/5
        public ActionResult Edit(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            neighborhood neighborhood = db.neighborhood.Find(id);
            if (neighborhood == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcity_neighborhood = new SelectList(db.city, "id_city", "name_city", neighborhood.idcity_neighborhood);
            return View(neighborhood);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // POST: Manager_neighborhoods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_neighborhood,name_neighborhood,idcity_neighborhood")] neighborhood neighborhood)
        {
            try
            {
                if (ModelState.IsValid)
            {
                db.Entry(neighborhood).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcity_neighborhood = new SelectList(db.city, "id_city", "name_city", neighborhood.idcity_neighborhood);
            return View(neighborhood);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();

            }
        }

        // GET: Manager_neighborhoods/Delete/5
        public ActionResult Delete(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            neighborhood neighborhood = db.neighborhood.Find(id);
            if (neighborhood == null)
            {
                return HttpNotFound();
            }
            return View(neighborhood);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים"; ;
                return View();
            }
        }

        // POST: Manager_neighborhoods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{
                //מחיקת כל הנתונים המקושרים לשכונה זו
                neighborhood neighborhood = db.neighborhood.Find(id);
                db.children.RemoveRange(db.children.Where(c => c.client.neighborhood.id_neighborhood == id));
                db.tblfiles.RemoveRange(db.tblfiles.Where(c => c.client.neighborhood.id_neighborhood == id));
                db.order_details.RemoveRange(db.order_details.Where(c => c.orders.client.neighborhood.id_neighborhood == id));
                db.orders.RemoveRange(db.orders.Where(c => c.client.neighborhood.id_neighborhood == id));
                db.item_in_wait.RemoveRange(db.item_in_wait.Where(c => c.client.neighborhood.id_neighborhood == id));
                db.client.RemoveRange(db.client.Where(c => c.neighborhood.id_neighborhood == id));
                db.item_in_sale.RemoveRange(db.item_in_sale.Where(c => c.sale.neighborhood.id_neighborhood == id));
                db.sale.RemoveRange(db.sale.Where(c => c.neighborhood.id_neighborhood == id));
                db.neighborhood.Remove(neighborhood);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
