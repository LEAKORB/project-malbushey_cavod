﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;
using System.Data.Entity;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Project.Controllers
{
    public class HomeController : Controller
    {

        private P2Entities2 db = new P2Entities2();

 
            // GET: Home
            public ActionResult Index()
            {
            return View();
            }
            // פונקצית כניסת משתמש קיים   
            [HttpPost]
            public ActionResult login(string nameC, string tzC, string passwordC)
            {
                try
                {
                DBShopinCart.lShopingCard.Clear();
    
                Session.Add("tzC", tzC);
                    ViewBag.Name = nameC;                    
                    var c = db.client.FirstOrDefault(x => x.nameclient == nameC && x.tz_client.ToString() == tzC && x.passwordc.ToString() == passwordC);

                if (c != null)
                    {
                    Session.Add("idC", c.id_client);
                    if (c.users.name_user == "מנהל/ת")
                    {
                        Session.Add("nameC", nameC);

                        return RedirectToAction("Index", "Manager_settings");
                    }

                    var nChild = db.children.Count(a => a.idclient_child == c.id_client);
                    int nFile = db.tblfiles.Count(a => a.id_c == c.id_client);
                    if (c.numchildren_client == null) 
                        c.numchildren_client = 0;
                    // :בדיקה אם מספר ילדים בטבלת קלינט גדול ממספר הילדים המופיעים בטבלת ילדים כלומר 
                    // משתמש לא סיים את תהליך הרישום-הזין מספר ילדים אך יצא מהתכנית בלי להזין אותם בטבלת ילדים 
                    //מעבר להוספת ילדים במידה והכניסו מספר ילדים גדול ממה ששמור במערכת
                    if ( nChild < c.numchildren_client)
                    {
                        List<children> listChildren = new List<children>();
                        for (int i = 0; i < c.numchildren_client; i++)
                        {
                            children children = new children();
                            children.idclient_child = c.id_client;
                            listChildren.Add(children);
                        }
                        CustomerClientVM ccVM = new CustomerClientVM();
                        ccVM.listChlidren = listChildren;
                        ViewBag.error1 = "נא סיים את תהליך הרישום";
                        return View("~/Views/CustomerClients/CreateDetails.cshtml",  ccVM);
                    }
                    else
                    {
                        //מעבר להסרת ילדים שמעל גיל 18 במידה והכניסו מספר ילדים קטן ממה ששמור במערכת
                        if (nChild > c.numchildren_client)
                        {
                            int moneDel = 0;
                            var lChild = db.children.Where(a => a.idclient_child == c.id_client).ToList();
                            for (int i = 0; i < nChild; i++)
                            {
                                int age = DateTime.Now.Year - lChild[i].birthdate_child.Year;
                                if(age > 18)
                                {
                                    moneDel++;
                                    db.children.Remove(db.children.Find(lChild[i].id_child));
                                    db.SaveChanges();
                                }
                                else
                                {
                                    if (age == 18)
                                    {
                                        if (DateTime.Now.Month < lChild[i].birthdate_child.Month || DateTime.Now.Month == lChild[i].birthdate_child.Month && DateTime.Now.Day < lChild[i].birthdate_child.Day)
                                        {
                                            moneDel++;
                                            db.children.Remove(db.children.Find(lChild[i].id_child));
                                            db.SaveChanges();
                                        }
                                    }

                                }  

                            }//במידה והמערכת זיהתה מספר ילדים מעל גיל 18 גדול יותר ממה שהמשתמש הזין 
                            if (moneDel > nChild - c.numchildren_client)
                            {
                                var entity = db.client.Where(b => b.id_client == c.id_client).AsQueryable().FirstOrDefault();
                                c.numchildren_client = nChild - moneDel;
                                db.Entry(entity).CurrentValues.SetValues(c);
                            }
                            if(moneDel==0)
                            {    //אם לא מצא ילדים מתחת גיל 18
                                ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
                                ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                                ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                                ViewBag.error = "מס' ילדים מעל גיל 18 שגוי";
                                return View("~/Views/CustomerClients/Edit.cshtml", c);
                            }
                        }//אם אינו עומד בקרקטריונים לאחר עדכון הפרטים
                        int minFamsize = db.setting.Min(mi => mi.minfamsize_setting);
                        if (c.numchildren_client < minFamsize)
                        {
                            ViewBag.error = "אינך זכאי";
                            return View("~/Views/CustomerClients/Out.cshtml");
                        }
                    }
                        // :בדיקה אם אין למשתמש קבצים מצורפים כלומר 
                        // משתמש לא סיים את תהליך הרישום - יצא מהתכנית בלי לצרף קבצים 
                        if (nFile==0)
                    {
                        ViewBag.error1 = "נא סיים את תהליך הרישום";
                        return View("~/Views/Customer_children/Upload_files.cshtml");
                    }
                    //אם כל הפרטים של המשתמש מלאים נשלח לדף עדכון פרטים שקימים במערכת
                    if (c.users.name_user == "קונה")
                    {
                        ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
                        ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
                        ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
                        return View("~/Views/CustomerClients/Edit.cshtml", c);
                    }
                    }
                    else
                        ViewBag.error1 = "אינך קיים, הרשם עכשיו";
                }
                catch
                {
                    ViewBag.error1 = "ארעה שגיאה, נסה שוב";
                }
            ViewBag.Name = nameC;
            return View("Index");
            }
            //פונקציה לכניסת משתמש חדש
            // GET: Home
            public ActionResult newUser()
            {
                return View();
            }
            [HttpGet]
            public ActionResult createNewUser(string nameC, string tzC, string passwordC)
            {
                try
                {
                    Session.Add("tz", tzC);
                    ViewBag.Name = nameC;
                Session.Add("name", nameC);
                Session.Add("password", passwordC);
                return RedirectToAction("Create", "CustomerClients");
                }
                catch
                {
                    ViewBag.error1 = "ארעה שגיאה, נסה שוב";
                }
                return View("Index");
            }
            // פונקצית איפוס סיסמה   
            // GET: Home
            public ActionResult password()
            {
                return View();
            }
            //GET:Home
            public ActionResult ResetPassword()
            {
                return View();
            }
            // פונקציה להפעלת סיסמה חדשה   
            [HttpGet]
            public ActionResult RestartPassword(string nameC, string tzC, string passwordC)
            {
                try
                {
                    Session.Add("tzC", tzC);
                    ViewBag.Name = nameC;
                    var c = db.client.FirstOrDefault(x => x.nameclient == nameC && x.tz_client.ToString() == tzC);
                    if (c != null)
                    {
                        c.passwordc = passwordC;
                        db.SaveChanges();
                        if (c.users.name_user == "מנהל/ת")
                            return RedirectToAction("Index", "Manager_clients");
                        if (c.users.name_user == "קונה")
                            return RedirectToAction("ContactUs", "Manager_items");
                        else
                            return RedirectToAction("Index", "Manager_sales");
                    }
                    else
                        ViewBag.error1 = "אינך קיים, הרשם עכשיו";
                }
                catch
                {
                    ViewBag.error1 = "ארעה שגיאה, נסה שוב";
                }
                return View("Index");
            }
            //פונקצית שליחת סיסמה למייל 
            public void sendEmailAsync(string ToEmail, string UserName, string nPassword, string link)/*string UniqeId(int id, string code)*/
            {
                try
                {

                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress("organizationmc1@gmail.com", "מלבושי כבוד");
                    mail.To.Add(ToEmail);
                    StringBuilder sbEmailbody = new StringBuilder();
                    sbEmailbody.Append("לכבוד " + UserName + ",");
                    sbEmailbody.Append("<br/><br/>");
                    sbEmailbody.Append(" : הסיסמה החדשה היא " + nPassword);
                    sbEmailbody.Append("<br/>");
                    sbEmailbody.Append(" על מנת לאתחל את הסיסמה החדשה <a href=\"" + link + "\">לחץ כאן</a>");
                    sbEmailbody.Append("<br/><br/>");
                    sbEmailbody.Append("<b>מלבושי כבוד<b>");
                    mail.IsBodyHtml = true;
                    mail.Body = sbEmailbody.ToString();
                    mail.Subject = "איפוס סיסמה";
                    SmtpClient smtpClient = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential("organizationmc1@gmail.com", "123456789mc")
                    };
                    smtpClient.Send(mail);
                }
                catch
                {
                    ViewBag.error1 = "ארעה שגיאה, נסה שוב";
                }
            }

            //פונקציה ליצירת סיסמה חדשה
            public async Task<ActionResult> createPassword(string emailadress, string password, string passwordC)
            {
                try
                {
                    var user = await db.client.FirstOrDefaultAsync(x => x.email_client.ToString() == emailadress);
                    if (user != null && password == passwordC)
                    {
                        ViewBag.link = "http://localhost:57611/Home/ResetPassword";
                        //
                        ViewBag.click = "הסיסמה החדשה היא: " + password + ",לחץ כאן לאתחל את הסיסמא";
                        ViewBag.message = "קוד המשתמש האישי שלך נשלח למייל, גש לשם להמשך";
                        sendEmailAsync(user.email_client, user.nameclient, passwordC, ViewBag.link);
                        return View("password");
                    }
                    ViewBag.error1 = "אינך קיים, הרשם עכשיו";
                    return View("password");

                }
                catch
                {
                    ViewBag.error1 = "ארעה שגיאה, נסה שוב";
                }
                return View("Index");
            }
        }
    }


