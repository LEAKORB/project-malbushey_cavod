﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;
using System.ComponentModel.DataAnnotations;

using System.Net.Mail;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Drawing;

namespace Project.Controllers
{
    public class IdShoppesc
    {
        public static int IdShoppes = 0;
    };
    public class Manager_clientsController : Controller
    {
        private P2Entities2 db = new P2Entities2();
        int check = 0;
        // המציג למשתמש את פרטיו בסיום קניה view
        [HttpGet]
        public ActionResult CreateShopper()
        {
            var IdCustomer = Session["password"];
            if (IdCustomer != null)
            {
                var old = db.client.FirstOrDefault(x => x.id_client.ToString() == IdCustomer.ToString());

                if (old != null)
                {
                    ViewBag.NameCustomer = old.nameclient;
                    ViewBag.PasswordCustomer = old.id_client;
                    ViewBag.PhoneCustomer = old.phone_client;
                    ViewBag.AddressCustomer = old.address_client;

                }
                else
                {//במקרה שהמשתמש לא קיים- הצגת הודעת שגיאה
                    ViewBag.error = "you don't exist";

                }
            }
            return View();
        }

        // GET: Manager_clients
        public ActionResult Index()
        {
            var client = db.client.Include(c => c.city).Include(c => c.neighborhood).Include(c => c.users);
            return View(client.ToList());
        }

        // GET: Manager_clients/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.client.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: Manager_clients/Create
        public ActionResult Create()
        {
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city");
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood");
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user");
            return View();
        }
        
        // POST: Manager_clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_client,iduser_client,tz_client,address_client,idcity_client,idneighborhood_client,phone_client,fatherjob_client,avgsalary_client,email_client,nameclient,lastorder_client,numchildren_client,tz_client,passwordc,salary1,salary2,salary3")] client client)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    client.avgsalary_client = (Convert.ToInt16(client.salary1) + Convert.ToInt16(client.salary2) + Convert.ToInt16(client.salary3)) / 3;
                    db.client.Add(client);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city", client.idcity_client);
                ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", client.idneighborhood_client);
                ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user", client.iduser_client);
                return View(client);
            } 
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_clients/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.client.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city", client.idcity_client);
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", client.idneighborhood_client);
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user", client.iduser_client);
            return View(client);
        }

        // POST: Manager_clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_client,iduser_client,address_client,idcity_client,idneighborhood_client,phone_client,fatherjob_client,avgsalary_client,email_client,nameclient,lastorder_client,numchildren_client,tz_client,passwordc,salary1,salary2,salary3")] client client)
        {
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city", client.idcity_client);
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", client.idneighborhood_client);
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user", client.iduser_client);
            if (ModelState.IsValid)
            {
                client.avgsalary_client = (Convert.ToInt16(client.salary1) + Convert.ToInt16(client.salary2) + Convert.ToInt16(client.salary3)) / 3;
                db.Entry(client).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idcity_client = new SelectList(db.city, "id_city", "name_city", client.idcity_client);
            ViewBag.idneighborhood_client = new SelectList(db.neighborhood, "id_neighborhood", "name_neighborhood", client.idneighborhood_client);
            ViewBag.iduser_client = new SelectList(db.users, "id_user", "name_user", client.iduser_client);
            return View(client);
        }

        // GET: Manager_clients/Delete/5
        public ActionResult Delete(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            client client = db.client.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try {
                client client = db.client.Find(id);
                db.children.RemoveRange(db.children.Where(c => c.idclient_child == id));
                db.tblfiles.RemoveRange(db.tblfiles.Where(c => c.id_c == id));
                db.client.Remove(client);
                db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }
        //סיום הזמנה ושליחת ההזמנה למייל
        public ActionResult endShop()
        {
            var IdCustomer = Session["tzC"].ToString();
            //הצגת המחיר הסופי ללקוח
            ViewBag.s = DBShopinCart.lShopingCard.Sum(x => x.price_item * x.Amount);
            //הצגת התאריך הנוכחי ללקוח
            ViewBag.d = DateTime.Today;
            var s = db.client.FirstOrDefault(x => x.tz_client.ToString() == IdCustomer.ToString());
            if (IdCustomer != null && s.address_client != null)
            { //הוספת קניה חדשה לרשימת הקניות בעת קניה
                orders ts = new orders();
                ts.date_order = DateTime.Today;
                ts.idclient_order = s.id_client;
                db.orders.Add(ts);
                db.SaveChanges();
                int kod = ts.id_order;
                myStatistics mys = new myStatistics();
                List<MyShoppingCart> shop = new List<MyShoppingCart>();
                shop = DBShopinCart.lShopingCard.Where(y => y.itemInWait != 1).ToList();
                foreach (var item in shop)
                {
                    var ise = db.item_in_sale.Find(item.id_item);
                    order_details sd = new order_details();
                    sd.amount_details= item.Amount;
                    sd.iditem_details = ise.item.id_item;
                    sd.idorder_details = kod;
                    sd.size_details =Convert.ToInt32(item.sizeinstock_item);
                    sd.price = (float)item.price_item;
                    db.order_details.Add(sd);
                    db.SaveChanges();
                    mys.amount = item.Amount;
                    mys.ids_neighborhood = s.neighborhood.id_neighborhood;
                    mys.id_city = s.city.id_city;
                    mys.id_item = ise.item.id_item;
                    mys.id_type = ise.item.type_item.id_type;
                    mys.last_order = DateTime.Today;
                    mys.size =Convert.ToInt32( item.sizeinstock_item);
                    mys.id_category = ise.item.category_item.id_category;
                    db.myStatistics.Add(mys);
                    db.SaveChanges();
                }
                sentEmail(-1);

        }
            else
            {//הודעת שגיאה במקרה הלקוח לא הכניס את פרטיו
                ViewBag.error1 = "Put in your details";
                ViewBag.error2 = "for old shopper put your password";
            }
            return View();
        }

        public ActionResult sentEmail(int id)
        {
            try
            {
                //שליחת ההזמנה למייל 
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("organizationmc1@gmail.com", "מלבושי כבוד");
                mail.To.Add("miriamyahud@gmail.com");
                mail.Subject = "מספר הזמנה - לא להשיב";
                mail.Body = "לקוח יקר להלן פרטי הזמנתך" + ":";
                check = 1;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential("organizationmc1@gmail.com", "123456789mc")
                };
                DownloadPDF(id);
                mail.Attachments.Add(new Attachment(@"C:/Users/1/Documents/OrderDetails.pdf"));
                //mail.Attachments.Add(new Attachment(@"G:/Esther Chana/Try Orders Details.pdf"));
                smtp.Send(mail);
                ViewBag.success = "ההזמנה נשלחה למייל השמור במאגרנו";
            }
            catch
            {
                ViewBag.error1 = ".היתה בעיה בשליחת ההזמנה למייל";
            }
            if (id != -1)
            {
                var model = new modelOfItemAndCategory
                {
                    Items = db.item_in_sale.Include(i => i.item.type_item).ToList(),
                    Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                    Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                    MyShoppingCart = DBShopinCart.lShopingCard,
                    orders = db.orders.Include(o => o.client).ToList(),
                    order_details = db.order_details.Where(e => e.idorder_details == id).ToList()
                };
                return View("~/Views/Manager_items/DetailsOrders.cshtml", model);
            }
            else
            {
                
                var IdCustomer = Session["idC"];
                Manager_itemsController.delCookie(IdCustomer.ToString());
                var cli = db.client.Find(Convert.ToInt32(IdCustomer));
                var model = new modelOfItemAndCategory
                {
                    Items = db.item_in_sale.Where(i => i.sale.idcity_sale == cli.idcity_client && i.sale.idneighborhood_sale == cli.idneighborhood_client).ToList(),
                    Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                    Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                    orders = db.orders.Include(o => o.client).ToList(),
                    item_in_wait = db.item_in_wait.Where(e => e.idorder_wait == id).ToList()
                };
                return View("endShop",model);
            }
            }
        //אפשרות הורדת ההזמנה למחשב ישירות
        
        public ActionResult DownloadPDF(int id)
        {
            
            using (MemoryStream ms = new MemoryStream())
            {
                Document doc = new Document(iTextSharp.text.PageSize.A4, 88f, 88f, 10f, 10f);

                BaseFont bf = BaseFont.CreateFont(Server.MapPath("~/Fonts/GISHA.ttf"), BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                BaseFont bf1 = BaseFont.CreateFont(Server.MapPath("~/Fonts/GISHA.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                iTextSharp.text.Font gisha = new iTextSharp.text.Font(bf, 12);
                iTextSharp.text.Font gisha1 = new iTextSharp.text.Font(bf, 14);
                iTextSharp.text.Font gishaH = new iTextSharp.text.Font(bf1, 14, iTextSharp.text.Font.BOLD);
                
                if (check == 0)
                {
                    PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                }
                else
                {
                    PdfWriter writer1 = PdfWriter.GetInstance(doc, new FileStream(@"C:/Users/1/Documents/OrderDetails.pdf", FileMode.Create));
                }
                
                doc.Open();
                gisha.SetColor(0, 0, 128);
                gishaH.SetColor(0, 0, 128);
                gisha1.SetColor(0, 0, 128);
                string imageURL = "C:/Users/1/Desktop/111118/Project/Project/pictures//logoblue.JPG";
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);

                //גודל התמונה
                jpg.ScaleToFit(110f, 110f);
                //רווח אחרי התמונה
                jpg.SpacingBefore = 10f;
                //רווח לאחרי התמונה
                jpg.SpacingAfter = 1f;
                //מיקום התמונה
                jpg.Alignment = Element.ALIGN_CENTER;
                doc.Add(jpg);
                
                PdfPTable table = new PdfPTable(1);
                table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                PdfPCell cell = new PdfPCell();

                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell = new PdfPCell(new Phrase(Environment.NewLine + Environment.NewLine + "תאריך הזמנה" + ": " + DateTime.Today
                    + Environment.NewLine + Environment.NewLine, gisha));
                cell.BorderColor = BaseColor.WHITE;

                table.AddCell(cell);
                doc.Add(table);
                table.Rows.Clear();


                cell = new PdfPCell(new Phrase("לכבוד" + Environment.NewLine, gishaH));
                cell.BorderColor = BaseColor.WHITE;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase(db.client.FirstOrDefault(a => a.id_client == 1).nameclient
                    + Environment.NewLine + db.client.FirstOrDefault(a => a.id_client == 1).phone_client
                    + Environment.NewLine + db.client.FirstOrDefault(a => a.id_client == 1).email_client +
                    Environment.NewLine + db.client.FirstOrDefault(a => a.id_client == 1).neighborhood.name_neighborhood + " " + db.client.FirstOrDefault(a => a.id_client == 1).address_client
                    + Environment.NewLine + db.client.FirstOrDefault(a => a.id_client == 1).city.name_city+ Environment.NewLine, gisha1));
                cell.BorderColor = BaseColor.WHITE;
                cell.SpaceCharRatio = 1f;
                table.AddCell(cell);

                cell = new PdfPCell(new Phrase(Environment.NewLine + Environment.NewLine + "להלן סיכום חשבונך," + Environment.NewLine + Environment.NewLine, gishaH));
                cell.BorderColor = BaseColor.WHITE;
                table.AddCell(cell);

                doc.Add(table);

                table.Rows.Clear();

                table = new PdfPTable(4);

                table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                cell = new PdfPCell(new Phrase("מוצר" + ": " + Environment.NewLine, gisha1));
                cell.BorderColor =  new BaseColor(219, 112, 147);
                cell.BackgroundColor = new BaseColor(219, 112, 147);
                table.AddCell(cell);



                cell = new PdfPCell(new Phrase("מידה" + ": " + Environment.NewLine, gisha1));
                cell.BorderColor = new BaseColor(219, 112, 147);
                cell.BackgroundColor = new BaseColor(219, 112, 147);
                table.AddCell(cell);

                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell = new PdfPCell(new Phrase("כמות" + ": " + Environment.NewLine, gisha1));
                cell.BorderColor = new BaseColor(219, 112, 147);
                cell.BackgroundColor = new BaseColor(219, 112, 147);
                table.AddCell(cell);


                cell.BorderWidth = 0;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                cell = new PdfPCell(new Phrase("מחיר" + ": " + Environment.NewLine, gisha1));
                cell.BorderColor = new BaseColor(219, 112, 147);
                cell.BackgroundColor = new BaseColor(219, 112, 147);
                table.AddCell(cell);
                List<MyShoppingCart> shop = new List<MyShoppingCart>();
                if (id == -1)
                {
                     shop = DBShopinCart.lShopingCard.Where(y => y.itemInWait != 1).ToList();
                }
                else
                {
                    var or= db.order_details.Where(i => i.idorder_details == id).ToList();
                    foreach (var item in or)
                    {
                       
                        MyShoppingCart m = new MyShoppingCart()
                        {
                            id_item = item.item.id_item,
                            Amount = item.amount_details,
                            price_item = item.item.price_item,
                            sizeinstock_item = item.size_details.ToString(),
                            type_item = item.item.type_item,
                            color_item = item.item.color_item,
                            limitedamount_item = item.item.limitedamount_item,
                            name_item = item.item.name_item,
                            picture_item = item.item.picture_item,
                        };
                        shop.Add(m);
                    }
                }
                decimal sp = shop.Sum(x => x.price_item * x.Amount);
                for (int i = 0; i < shop.Count; i++)
                {


                    cell = new PdfPCell(new Phrase(shop[i].name_item, gisha1));
                    cell.BackgroundColor = new BaseColor(255, 239, 213);
                    cell.BorderColorTop = new BaseColor(255, 239, 213);
                    cell.BorderColor = new BaseColor(255, 239, 213);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(shop[i].sizeinstock_item.ToString(), gisha1));
                    cell.BackgroundColor = new BaseColor(255, 239, 213);
                    cell.BorderColorTop = new BaseColor(255, 239, 213);
                    cell.BorderColor = new BaseColor(255, 239, 213);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(shop[i].Amount.ToString(), gisha1));
                    cell.BackgroundColor = new BaseColor(255, 239, 213);
                    cell.BorderColorTop = new BaseColor(255, 239, 213);
                    cell.BorderColor = new BaseColor(255, 239, 213);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Phrase(System.Math.Round(shop[i].price_item, 2).ToString() , gisha1));
                    cell.BackgroundColor = new BaseColor(255, 239, 213);
                    cell.BorderColorTop = new BaseColor(255, 239, 213);
                    cell.BorderColor = new BaseColor(255, 239, 213);

                    table.AddCell(cell);
                }
                doc.Add(table);
                doc.Add(Chunk.NEWLINE);

                table.Rows.Clear();
                table = new PdfPTable(2);
                table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                cell = new PdfPCell(new Phrase(Environment.NewLine+"סה\"כ עלות" + ": " + Environment.NewLine, gishaH));
                cell.BorderColor = BaseColor.WHITE;
                table.AddCell(cell);


                cell = new PdfPCell(new Phrase(Environment.NewLine + System.Math.Round(sp,2) + Environment.NewLine, gishaH));
                cell.BorderColor = BaseColor.WHITE;
                cell.BorderColorBottom = new BaseColor(219, 112, 147);
                table.AddCell(cell);
                doc.Add(table);
                table = new PdfPTable(1);
                table.Rows.Clear();
                table.RunDirection = PdfWriter.RUN_DIRECTION_RTL;


                cell = new PdfPCell(new Phrase(Environment.NewLine + "אנו מודים לך על שהזמנת אצלנו" +
                    Environment.NewLine + Environment.NewLine
                   , gisha1));
                cell.BorderColor = BaseColor.WHITE;
                table.AddCell(cell);
                doc.Add(table);
                table.Rows.Clear();

                doc.Close();


                byte[] bytes = ms.ToArray();
                ms.Close();
                if (check == 0)
                    return File(bytes, "application/pdf", "Order.pdf");
                else
                {
                    check = 0;
                    return View();

                }
            }
        }

        //חיפוס פריט לפי שם 
        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {
            
            var res = db.client.Where(a => a.nameclient.Contains(searchTxt) || a.tz_client.Contains(searchTxt)).ToList();
            if (res.Count() != 0) { 
            return PartialView(res);
            }
            else
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                return PartialView(res);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
