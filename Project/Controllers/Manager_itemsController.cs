﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Project.Models;


namespace Project.Controllers
{
    public class Manager_itemsController : Controller
    {
        private P2Entities2 db = new P2Entities2();
        // GET: items
        public ActionResult Index()
        {
            var item = db.item.Include(i => i.type_item).Include(i => i.category_item);
            return View(item.ToList());
        }
        
        // GET: items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item_in_sale item = db.item_in_sale.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            List<int> s = new List<int>();
            int index1 = item.item.sizeinstock_item.IndexOf('-');
            if (index1 != -1)
            {
                string start = item.item.sizeinstock_item.Substring(0, item.item.sizeinstock_item.LastIndexOf("-"));
                string end = item.item.sizeinstock_item.Remove(0, item.item.sizeinstock_item.LastIndexOf("-") + 1);
                // מידות לפרטי לבוש זוגי בלבד
                if (item.item.id_i_category != 4)
                {

                    for (int i = Convert.ToInt16(start); i <= Convert.ToInt16(end); i += 2)
                    {
                        s.Add(i);
                    }
                }
                else
                {//מידות לנעליים
                    for (int i = Convert.ToInt16(start); i <= Convert.ToInt16(end); i++)
                    {
                        s.Add(i);
                    }
                }

            }
            else
            s.Add(Convert.ToInt16(item.item.sizeinstock_item));
            List<item_in_sale> l = GetLastFromCookie();
            var find = l.FirstOrDefault(a=>a.id_itemsale==item.id_itemsale);
            if (find == null)
            {
                if (l.Count == 4)
                {
                    l.Remove(l.First());
                }
                l.Add(item);
                delCookie(Session["tzC"].ToString());
                var IdCustomer = Session["tzC"];
                SetLastCookie(l, IdCustomer.ToString());
            }
            ViewBag.size = new SelectList(s);
            return View(item);
        }
        // GET: items/Details/5
        public ActionResult DetailsMan(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = db.item.Where(t=>t.id_item==id).ToList();

                if (item == null)
            {
                return HttpNotFound();
                    
                }
                return View(item);
        }

        // GET: items/Create
        public ActionResult Create()
        {
            ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type");
            ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category");
            return View();
        }

        // POST: items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_item,name_item,idtype_item,picture_item,sizeinstock_item,price_item,color_item,limitedamount_item,id_i_category")] item item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.item.Add(item);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type", item.idtype_item);
                ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category", item.id_i_category);
                return View(item);
            } 
            catch
            {
                ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type");
                ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category");
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: items/Edit/5
        public ActionResult Edit(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item item = db.item.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type", item.idtype_item);
            ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category", item.id_i_category);
            return View(item);
            }
            catch
            {
                ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type");
                ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category");
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // POST: items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_item,name_item,idtype_item,picture_item,sizeinstock_item,price_item,color_item,limitedamount_item,id_i_category")] item item)
        {
          try{
                if (ModelState.IsValid)
                {
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
            }
            ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type", item.idtype_item);
            ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category", item.id_i_category);
            return View(item);
            }
            catch
            {
                ViewBag.idtype_item = new SelectList(db.type_item, "id_type", "name_type");
                ViewBag.id_i_category = new SelectList(db.category_item, "id_category", "name_category");
                ViewBag.error = ".ארעה שגיאה בעדכון הפרטים";
                return View();
            }
        }

        // GET: items/Delete/5
        public ActionResult Delete(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item item = db.item.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{//מחיקת כל הנתונים המקושרים לפריט זה
                item item = db.item.Find(id);
                db.order_details.RemoveRange(db.order_details.Where(c => c.item.id_item == id));
                db.item_in_sale.RemoveRange(db.item_in_sale.Where(c => c.item.id_item == id));
                db.item_in_wait.RemoveRange(db.item_in_wait.Where(c => c.item.id_item == id));
                db.item.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //----------------------------Shopper----------------------------------------
        // GET: TBL_Jewelry
        // המציג את פרטי המכירה view
        
        public ActionResult ContactUs()
        {
            var IdCustomer = Session["idC"];
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            var sale = db.sale.FirstOrDefault(a => a.idcity_sale == cli.idcity_client && a.idneighborhood_sale == cli.idneighborhood_client);
            ViewBag.address = "המכירה תתקיים ברחוב " + sale.addressSale + " " + sale.neighborhood.name_neighborhood + " " + sale.city.name_city;
            ViewBag.dat = sale.lod_sale.ToString().Substring(0, 10) + " - " + sale.fod_sale.ToString().Substring(0, 10);
            if (sale.lod_sale < DateTime.Now)
            {
                delCookie(Session["tzC"].ToString());
                delCookie(IdCustomer.ToString());
            }

            var model = new modelOfItemAndCategory
            {
                Items = GetLastFromCookie(),
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList()
            };

            return View(model);
        }
        // menu תצוגה חלקית לפי בחירת קטגוריה וסוג פריט ע"י המשתמש מה 
        public PartialViewResult ShopperIndex(string nameCategory, string type)
        {
            var IdCustomer = Session["idC"];
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            var model = new modelOfItemAndCategory
            {
                Items = db.item_in_sale.Where(a => a.item.type_item.name_type == type && a.item.category_item.name_category == nameCategory
                && a.sale.idcity_sale == cli.idcity_client && a.sale.idneighborhood_sale == cli.idneighborhood_client &&  a.sale.lod_sale >= DateTime.Now).ToList(),
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList()
            };
            if (model.Items.Count() == 0)
                ViewBag.error = "המכירה הסתיימה";
            return PartialView(model);
        }
        //פונקציה השולפת הפריטים בסל קניות שנשמרו ב cookies
        public List<MyShoppingCart> GetCartFromCookie()
        {
            var IdCustomer = Session["idC"].ToString();
            string text = System.Web.HttpContext.Current.Request.Cookies[IdCustomer] == null|| System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value == null||System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value == "" ? string.Empty : System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value.ToString();
            List<MyShoppingCart> cart=new List<MyShoppingCart>();
            if (text != "")
            {
                 cart = GetCart(text);
            }
            return cart;
        }
        //פונקציה שמירת הפריטים שבסל קניות ב cookies
        public static void SetCookie(List<MyShoppingCart> cart, string  IdCustomer )
        {
            string text = ToString(cart);
            HttpCookie cookie = new HttpCookie(IdCustomer, text);
            cookie.Expires = DateTime.Now.AddDays(30);
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }
        //פונקציה שמירת פריטים שנצפו לאחרונה ב cookies
        public static void SetLastCookie(List<item_in_sale> id, string IdCustomer)
        {

            string text = ToStringL(id);
            HttpCookie cookie = new HttpCookie(IdCustomer, text);
            cookie.Expires = DateTime.Now.AddDays(30);
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }
        //פונקציה המוחקת את כל המוצרים שנשמרו ב cookies
        public static void delCookie(string IdCustomer)
        {
            if (System.Web.HttpContext.Current.Response.Cookies[IdCustomer] != null)
            {
                System.Web.HttpContext.Current.Response.Cookies[IdCustomer].Value = "";
                System.Web.HttpContext.Current.Response.Cookies[IdCustomer].Expires = DateTime.Now.AddYears(-50);
                System.Web.HttpContext.Current.Response.Cookies.Add(System.Web.HttpContext.Current.Response.Cookies[IdCustomer]);
            }

            if (System.Web.HttpContext.Current.Request.Cookies[IdCustomer] != null)
            {
                System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value = "";
                System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Expires = DateTime.Now.AddYears(-50);
                System.Web.HttpContext.Current.Request.Cookies.Add(System.Web.HttpContext.Current.Request.Cookies[IdCustomer]);
            }
        }
        //פונקציה הממירה פריטים מסל קניות ל string  
        public static string ToString(List<MyShoppingCart> cart)
        {
            StringBuilder sb = new StringBuilder();
            if (cart != null)
            {
                for (int i = 0; i < cart.Count; i++)
                {
                    var item = cart[i];
                    sb.Append(item.id_item);
                    sb.Append(",");
                    sb.Append(item.sizeinstock_item);
                    sb.Append(",");
                    sb.Append(item.Amount);
                    sb.Append(",");
                    sb.Append(item.price_item);
                    sb.Append(",");
                    sb.Append(item.itemInWait);
                    if (i < cart.Count - 1)
                    {
                        sb.Append("|");
                    }
                }
            }
            return sb.ToString();
        }
        //פונקציה הממירה פריטים אחרונים ל string  
        public static string ToStringL(List<item_in_sale> id)
        {
            StringBuilder sb = new StringBuilder();
            if (id != null)
            {
                for (int i = 0; i < id.Count; i++)
                {
                    var item = id[i];
                    sb.Append(item.id_itemsale.ToString());
                    sb.Append(",");
                    if (i < id.Count - 1)
                    {
                        sb.Append("|");
                    }
                }
            }
            return sb.ToString();
        }
        //פונקציה השולפת הפריטים שנצפו אחרונים שנשמרו ב cookies
        public List<item_in_sale> GetLastFromCookie()
        {
            var IdCustomer = Session["tzC"].ToString();
            string text = System.Web.HttpContext.Current.Request.Cookies[IdCustomer] == null || System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value == null|| System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value == ""  ? string.Empty : System.Web.HttpContext.Current.Request.Cookies[IdCustomer].Value.ToString();
            List<item_in_sale> Last = new List<item_in_sale>();
            if (text != "")
            {
                Last = GetLast(text);
            }
            return Last;
        }
        //stringל cookies המרת פרטי סל קניות שנשמרו ב 
        public List<item_in_sale> GetLast(string text)
        {
            List<item_in_sale> Last = new List<item_in_sale>();

            string[] arr = text.Split('|');
            foreach (string item in arr)
            {
                int id = Convert.ToInt32(item.Split(',')[0]);
                var isale = db.item_in_sale.FirstOrDefault(a => a.id_itemsale == id);
                if(isale!=null)
                Last.Add(isale);
            }
            return Last;
        }
        //MyShoppingCartל cookies המרת פרטי סל קניות שנשמרו ב 
        public List<MyShoppingCart> GetCart(string text)
        {
            List<MyShoppingCart> cart = new List<MyShoppingCart>();

            string[] arr = text.Split('|');
            foreach (string item in arr)
            {
                int id = Convert.ToInt32(item.Split(',')[0]);
                string siz = item.Split(',')[1];
                int sizint = Convert.ToInt32(item.Split(',')[1]);
                var  q = db.size.FirstOrDefault(x => x.id_item ==id  && x.n_size.ToString() == siz);
                MyShoppingCart citem = new MyShoppingCart();
                citem.id_item =Convert.ToInt32(item.Split(',')[0]);
                citem.sizeinstock_item = item.Split(',')[1];
                citem.Amount = Convert.ToInt32(item.Split(',')[2]);
                citem.price_item = decimal.Parse(item.Split(',')[3]);
                citem.type_item = q.item_in_sale.item.type_item;
                citem.color_item = q.item_in_sale.item.color_item;
                citem.limitedamount_item = q.item_in_sale.item.limitedamount_item;
                citem.name_item = q.item_in_sale.item.name_item;
                citem.picture_item = q.item_in_sale.item.picture_item;
                citem.itemInWait = Convert.ToInt32(item.Split(',')[4]); ;
                var si = db.size.FirstOrDefault(t => t.id_item == id && t.n_size == sizint);
                cart.Add(citem);
            }
            return cart;
        }

        [HttpPost]
        //פונקצית הוספה לסל הקניות
        public ActionResult AddToCart(item_in_sale item,string quantity,string size)
        {
            if (DBShopinCart.lShopingCard.Count == 0)
            {
                DBShopinCart.lShopingCard = GetCartFromCookie();
            }
            var id = item.id_itemsale;
            var q = db.size.FirstOrDefault(x => x.id_item == id && x.n_size.ToString()==size);
            int ezSize = Convert.ToInt16(size);
            var l = DBShopinCart.lShopingCard.FirstOrDefault(y => y.id_item == id && y.sizeinstock_item == size&&y.itemInWait == 0);
            var itemWait = 0;
            var si = db.size.FirstOrDefault(t => t.id_item == id && t.n_size ==ezSize);
            if (si!=null)
            {      
                    if (si.amount == 0||si.amount- Convert.ToInt16(quantity)<0)
                    {
                    l = DBShopinCart.lShopingCard.FirstOrDefault(y => y.id_item == id && y.sizeinstock_item == size && y.itemInWait == 1);
                    itemWait = 1;
                    }
                    else
                    {
                    itemWait = 0;
                    si.amount = si.amount - Convert.ToInt16(quantity);
                    db.Entry(si).State = EntityState.Modified;
                      db.SaveChanges();
                    }

                if (l == null)
                {
                    MyShoppingCart s = new MyShoppingCart()
                    {
                        id_item = q.item_in_sale.id_itemsale,
                        Amount = Convert.ToInt16(quantity),
                        price_item = q.item_in_sale.item.price_item,
                        sizeinstock_item = si.n_size.ToString(),
                        type_item = q.item_in_sale.item.type_item,
                        color_item = q.item_in_sale.item.color_item,
                        limitedamount_item = q.item_in_sale.item.limitedamount_item,
                        name_item = q.item_in_sale.item.name_item,
                        picture_item = q.item_in_sale.item.picture_item,
                        itemInWait=itemWait                    };
                        DBShopinCart.lShopingCard.Add(s);
                }
                else
                {
                    l.Amount += Convert.ToInt16(quantity);
                    l.price_item += q.item_in_sale.item.price_item;
                }
                var IdCustomer = Session["idC"].ToString();
                delCookie(IdCustomer);
                SetCookie(DBShopinCart.lShopingCard, IdCustomer);
                return RedirectToAction("ContactUs");

            }
            return View();
        }
        
        // הצגת הזמנות קודמות למשתמש
        public ActionResult prevOrders()
        {
            var IdCustomer = Session["idC"].ToString();
            var or = db.orders.Where(o => o.idclient_order.ToString()==IdCustomer).ToList();
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            List<orders> buyO = new List<orders>();
            foreach (var item in or)
            {
                var bu = db.order_details.FirstOrDefault(a => a.idorder_details == item.id_order);
                if (bu != null)
                    buyO.Add(item);
            }
            var model = new modelOfItemAndCategory
            {
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                orders = buyO.ToList()
            };
            return View(model);
        }
        // הצגת הזמנות בהמתנה למשתמש
        public ActionResult WaitOrders()
        {
            var IdCustomer = Session["idC"].ToString();
            var or = db.orders.Where(o => o.idclient_order.ToString() == IdCustomer).ToList();
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            List<orders> waitO = new List<orders>();
            foreach (var item in or)
            {
                var wai = db.item_in_wait.FirstOrDefault(a => a.idorder_wait == item.id_order);
                if (wai != null)
                    waitO.Add(item);
            }
            var model = new modelOfItemAndCategory
            {
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                orders = waitO.ToList()
            };
            return View(model);
        }
        // הצגת  פירוט הזמנות למשתמש
        public ActionResult DetailsOrders(int id)
        {
            var model = new modelOfItemAndCategory
            {
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                orders = db.orders.Include(o => o.client).ToList(),
                order_details = db.order_details.Where(e => e.idorder_details == id).ToList()
            };
            return View(model);
        }
      
        // הצגת  פירוט הזמנות בהמתנה למשתמש
       public ActionResult DetailsWait(int id)
        {
            var model = new modelOfItemAndCategory
            {
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                orders = db.orders.Include(o => o.client).ToList(),
                item_in_wait = db.item_in_wait.Where(e => e.idorder_wait == id).ToList()
            };
            return View(model);
        }

        // הצגת  סל קניות למשתמש
        public ActionResult MyShoppingCart()
        {
            

            var IdCustomer = Session["idC"];
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            var sale = db.sale.FirstOrDefault(a => a.idcity_sale == cli.idcity_client && a.idneighborhood_sale == cli.idneighborhood_client);
            if (DBShopinCart.lShopingCard.Count == 0&& sale.lod_sale >= DateTime.Now)
            {
                DBShopinCart.lShopingCard = GetCartFromCookie();
            }
            var model = new modelOfItemAndCategory
            {
                MyShoppingCart = GetCartFromCookie(),
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList()
            };
            return View(model);
        }

        
        //הוספת פריט לרשימת המתנה מסל הקניות
        [HttpGet]
        public ActionResult AddWait(string data)
        {
            var ez = data.Substring(0, data.IndexOf(";"));
            int id_n = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.Length-ez.Length-1);
            var q = DBShopinCart.lShopingCard.FirstOrDefault(y => y.id_item == id_n && y.sizeinstock_item == size && y.itemInWait == 1);
            var it = db.size.FirstOrDefault(y => y.item_in_sale.id_itemsale == id_n && y.n_size.ToString() == size);
            var item_inSale = db.item_in_sale.Find(id_n);
            id_n =Convert.ToInt32(item_inSale.iditems);        
            var i = Session["tzC"];
            var cli = db.client.FirstOrDefault(t => t.tz_client == i);
            var d = db.item_in_wait.FirstOrDefault(y => y.item.id_item == id_n && y.size_wait.ToString() == size && y.client.id_client==cli.id_client && y.date_wait == DateTime.Today);
            var ts = db.orders.FirstOrDefault(z => z.idclient_order == cli.id_client && z.date_order == DateTime.Today);
            if (ts == null)
            {
                ts = new orders() { id_order = ++IdShoppesc.IdShoppes, date_order = DateTime.Today, idclient_order = cli.id_client };
                db.orders.Add(ts);
                db.SaveChanges();
            }


            if (d == null)
            {
                item_in_wait s = new item_in_wait()
                {
                    amount_wait = q.Amount,
                    idclient_wait = cli.id_client,
                    iditem_wait= Convert.ToInt32(item_inSale.iditems),
                    price=(float) (q.Amount*it.item.price_item),
                    date_wait=DateTime.Today,
                    size_wait=Convert.ToInt16( q.sizeinstock_item),
                    idorder_wait=ts.id_order
                    
                };
                db.item_in_wait.Add(s);
            }
            else
            {
                d.idorder_wait = ts.id_order;
                d.amount_wait += 1;
                d.price +=(float) it.item.price_item;
                var entity = db.item_in_wait.FirstOrDefault(y => y.item.id_item == id_n && y.size_wait.ToString() == size && y.client.id_client == cli.id_client && y.date_wait == DateTime.Today);
                db.Entry(entity).CurrentValues.SetValues(d);
            }
            db.SaveChanges();
            q.itemInWait = 2;
            DBShopinCart.lShopingCard.Remove(q);
            var IdCustomer = Session["idC"].ToString();
            delCookie(IdCustomer);
            SetCookie(DBShopinCart.lShopingCard, IdCustomer);
            return RedirectToAction("MyShoppingCart");
        }
        // מחיקת כל הכמות מפריט הנמצא ברשימת המתנה
        [HttpGet]
        public ActionResult RemoveAllWait(string data)
        {
            var ez = data.Substring(0, data.IndexOf(";"));
            int id_n = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.LastIndexOf(";") - 3);
            DateTime date = DateTime.Parse(data.Substring(data.LastIndexOf(";") + 1, data.Length - ez.Length - size.Length - 2));
            var IdCustomer = Session["idC"];
            var clin = db.client.Find(Convert.ToInt32(IdCustomer));
            var sale = db.sale.FirstOrDefault(i => i.idcity_sale == clin.idcity_client && i.idneighborhood_sale == clin.idneighborhood_client);
            var d = db.item_in_wait.FirstOrDefault(y => y.iditem_wait == id_n && y.size_wait.ToString() == size && y.date_wait == date);
            if (d.orders.date_order <= sale.lod_sale && d.orders.date_order >= sale.fod_sale)
            {
                var id = d.idorder_wait;
                db.item_in_wait.Remove(d);
                db.SaveChanges();
                var od = db.orders.FirstOrDefault(o => o.id_order == id);
                var or_d = db.item_in_wait.Where(i => i.idorder_wait == od.idorder_detail);

                if (or_d.Count() == 0)
                {
                    db.orders.Remove(od);
                    db.SaveChanges();
                }
            }
            else
            {
                ViewBag.error = "אין אפשרות לבטל הזמנת פריטים אחרי שהמכירה הסתיימה";
            }
            var orderW = db.item_in_wait.Where(c => c.idorder_wait == d.idorder_wait).ToList();
            if (orderW.Count() > 0)
            {
                var model = new modelOfItemAndCategory
                {
                    Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                    Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                    item_in_wait = db.item_in_wait.Where(e => e.idorder_wait == d.idorder_wait && e.idclient_wait == d.idclient_wait && e.iditem_wait == d.iditem_wait).ToList()
                };
                return View("DetailsWait", model);
            }
            return RedirectToAction("WaitOrders");
        }
        
       // מחיקת פריט הנמצא בהזמנות
       [HttpGet]
        public ActionResult DeleteOr(string data)
        {

                var ez = data.Substring(0, data.IndexOf(";"));
                int id_n = Convert.ToInt16(ez);
                string size = data.Substring(data.IndexOf(";") + 1, data.LastIndexOf(";") - 3);
                int ord = Convert.ToInt32(data.Substring(data.LastIndexOf(";") + 1, data.Length - ez.Length - size.Length - 2));
                var IdCustomer = Session["idC"];
                var clin = db.client.Find(Convert.ToInt32(IdCustomer));
                var sale = db.sale.FirstOrDefault(i => i.idcity_sale == clin.idcity_client && i.idneighborhood_sale == clin.idneighborhood_client);
                var d = db.order_details.FirstOrDefault(y => y.iditem_details == id_n && y.size_details.ToString() == size && y.idorder_details == ord);
                if (d.orders.date_order <= sale.lod_sale && d.orders.date_order >= sale.fod_sale)
                {
                var si = db.size.FirstOrDefault(p => p.item_in_sale.iditems == id_n && p.n_size.ToString() == size);
                var listWait = db.item_in_wait.Where(s => s.iditem_wait == id_n && s.size_wait.ToString() == size).ToList();
                if (d.amount_details > 1)
                {
                    d.amount_details -= 1;
                    d.price -= (float)d.item.price_item;
                }
                else
                {
                    db.order_details.Remove(d);
                }
                si.amount = si.amount + 1;
                if (si.amount == 1)
                {
                        foreach (var item in listWait)
                        {
                            try
                            {
                                var cli = db.client.Find(item.idclient_wait);
                                if (cli != null)
                                {
                                    MailMessage mail = new MailMessage();
                                    mail.From = new MailAddress("organizationmc1@gmail.com", "מלבושי כבוד");
                                    mail.To.Add(cli.email_client);
                                    StringBuilder sbEmailbody = new StringBuilder();
                                    sbEmailbody.Append("לכבוד " + cli.nameclient + ",");
                                    sbEmailbody.Append("<br/><br/>");
                                    sbEmailbody.Append("המוצר " + item.item.name_item + "מידה " + size + " זמין להזמנה");
                                    sbEmailbody.Append("<br/><br/>");
                                    sbEmailbody.Append("<b>מלבושי כבוד<b>");
                                    mail.IsBodyHtml = true;
                                    mail.Body = sbEmailbody.ToString();
                                    mail.Subject = "מוצר בהמתנה";
                                    SmtpClient smtpClient = new SmtpClient
                                    {
                                        Host = "smtp.gmail.com",
                                        Port = 587,
                                        EnableSsl = true,
                                        DeliveryMethod = SmtpDeliveryMethod.Network,
                                        Credentials = new NetworkCredential("organizationmc1@gmail.com", "123456789mc")
                                    };
                                    smtpClient.Send(mail);
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                }
                else
                {
                    ViewBag.error = "אין אפשרות לבטל הזמנת פריטים אחרי שהמכירה הסתיימה";
                }
                db.SaveChanges();
            var orderD = db.order_details.Where(c => c.idorder_details == d.idorder_details).ToList();
            if (orderD.Count() > 0)
            {
                var model = new modelOfItemAndCategory
                {
                    Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                    Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                    order_details = db.order_details.Where(e => e.idorder_details == d.idorder_details && e.iditem_details == d.iditem_details).ToList()
                };
                return View("DetailsOrders", model);
            }
            db.orders.Remove(db.orders.FirstOrDefault(a => a.id_order == d.idorder_details));
            db.SaveChanges();
            return RedirectToAction("prevOrders");
        }
        // מחיקת כל הכמות מפריט הנמצא בהזמנות
        [HttpGet]
        public ActionResult RemoveAllOr(string data)
        {
                var ez = data.Substring(0, data.IndexOf(";"));
                var id_n = Convert.ToInt16(ez);
                string size = data.Substring(data.IndexOf(";") + 1, data.LastIndexOf(";") - 3);
                int ord = Convert.ToInt32(data.Substring(data.LastIndexOf(";") + 1, data.Length - ez.Length - size.Length - 2));
                var IdCustomer = Session["idC"];
                var clin = db.client.Find(Convert.ToInt32(IdCustomer));
                var sale = db.sale.FirstOrDefault(i => i.idcity_sale == clin.idcity_client && i.idneighborhood_sale == clin.idneighborhood_client);
                var d = db.order_details.FirstOrDefault(y => y.idorder_details == ord && y.iditem_details == id_n && y.size_details.ToString() == size);
                if (d.orders.date_order <= sale.lod_sale && d.orders.date_order >= sale.fod_sale)
                {
                    var si = db.size.FirstOrDefault(p => p.item_in_sale.iditems == id_n && p.n_size.ToString() == size);
                    var listWait = db.item_in_wait.Where(s => s.iditem_wait == id_n && s.size_wait.ToString() == size).ToList();
                    si.amount = si.amount + d.amount_details;
                    if (si.amount <= 0)
                    {
                        foreach (var item in listWait)
                        {
                            try
                            {
                                var cli = db.client.Find(item.idclient_wait);
                                if (cli != null)
                                {
                                    MailMessage mail = new MailMessage();
                                    mail.From = new MailAddress("organizationmc1@gmail.com", "מלבושי כבוד");
                                    mail.To.Add(cli.email_client);
                                    StringBuilder sbEmailbody = new StringBuilder();
                                    sbEmailbody.Append("לכבוד " + cli.nameclient + ",");
                                    sbEmailbody.Append("<br/><br/>");
                                    sbEmailbody.Append("המוצר " + item.item.name_item + "מידה " + size + " זמין להזמנה");
                                    sbEmailbody.Append("<br/><br/>");
                                    sbEmailbody.Append("<b>מלבושי כבוד<b>");
                                    mail.IsBodyHtml = true;
                                    mail.Body = sbEmailbody.ToString();
                                    mail.Subject = "מוצר בהמתנה";
                                    SmtpClient smtpClient = new SmtpClient
                                    {
                                        Host = "smtp.gmail.com",
                                        Port = 587,
                                        EnableSsl = true,
                                        DeliveryMethod = SmtpDeliveryMethod.Network,
                                        Credentials = new NetworkCredential("organizationmc1@gmail.com", "123456789mc")
                                    };
                                    smtpClient.Send(mail);
                                }

                            }
                            catch
                            {

                            }
                        }
                    }

                    db.order_details.Remove(d);
                    db.SaveChanges();
                }
                else
                {
                    ViewBag.error = "אין אפשרות לבטל הזמנת פריטים אחרי שהמכירה הסתיימה";
                }
            var orderD = db.order_details.Where(c => c.idorder_details == d.idorder_details).ToList();
            if (orderD.Count() > 0)
            {
                var model = new modelOfItemAndCategory
                {
                    Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                    Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                    order_details = db.order_details.Where(e => e.idorder_details == d.idorder_details && e.iditem_details == d.iditem_details).ToList()
                };
                return View("DetailsOrders", model);
            }
            db.orders.Remove(db.orders.FirstOrDefault(a => a.id_order == d.idorder_details));
            db.SaveChanges();
            return RedirectToAction("prevOrders");
        }
        //הוספת פריט סל הקניות
        [HttpGet]
        public ActionResult AddItem(string data)
        {
            var ez = data.Substring(0, data.IndexOf(";"));
            int id_n = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.Length-ez.Length-1);
            var d = DBShopinCart.lShopingCard.FirstOrDefault(y => y.id_item == id_n && y.sizeinstock_item.ToString() == size);
            var q = db.size.FirstOrDefault(p => p.item_in_sale.id_itemsale == id_n && p.n_size.ToString()==size);
            var si = db.size.FirstOrDefault(t => t.item_in_sale.id_itemsale == id_n && t.n_size.ToString() == size);
            if (d == null)
            {
                MyShoppingCart s = new MyShoppingCart()
                {
                    id_item = q.item.id_item,
                    Amount = 1,
                    price_item = q.item.price_item,
                    sizeinstock_item = q.item.sizeinstock_item,
                    type_item = q.item.type_item,
                    color_item = q.item.color_item,
                    limitedamount_item = q.item.limitedamount_item,
                    name_item = q.item.name_item,
                    picture_item = q.item.picture_item
                };
                DBShopinCart.lShopingCard.Add(s);
            }
            else
            {
                if ((d.Amount + 1) <= Convert.ToInt32(d.limitedamount_item))
                {
                    d.Amount += 1;
                    d.price_item += q.item_in_sale.item.price_item;
                }
            }
            si.amount = si.amount - 1;
            db.SaveChanges();
           
            var IdCustomer = Session["idC"].ToString();
            delCookie(IdCustomer);
            SetCookie(DBShopinCart.lShopingCard, IdCustomer);
            return RedirectToAction("MyShoppingCart");
        }
        //הוספת פריט לרשימת המתנה
        [HttpGet]
        public ActionResult AddWaitFromPlus(string data)
        {
            var ez = data.Substring(0, data.IndexOf(";"));
            int id_n = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.LastIndexOf(";") - 3);
            DateTime date = DateTime.Parse(data.Substring(data.LastIndexOf(";") + 1, data.Length - ez.Length-size.Length - 2));
            var IdCustomer = Session["idC"];
            var clin = db.client.Find(Convert.ToInt32(IdCustomer));
            var sale = db.sale.FirstOrDefault(s => s.idcity_sale == clin.idcity_client && s.idneighborhood_sale == clin.idneighborhood_client);

                int size_n = Convert.ToInt32(size);
            var q = db.item_in_wait.FirstOrDefault(y => y.item.id_item == id_n && y.size_wait == size_n&& y.date_wait==date);
            if (q.orders.date_order <= sale.lod_sale && q.orders.date_order >= sale.fod_sale)
            {
                var it = db.item.Find(id_n);
                var i = Session["tzC"];
                var cli = db.client.FirstOrDefault(t => t.tz_client == i);
                q.amount_wait += 1;
                q.price += (float)it.price_item;
                var entity = db.item_in_wait.FirstOrDefault(y => y.item.id_item == id_n && y.size_wait.ToString() == size && y.client.id_client == cli.id_client && y.date_wait == date);
                db.Entry(entity).CurrentValues.SetValues(q);
                db.SaveChanges();
            }
            else
            {
                ViewBag.error = "אין אפשרות להזמין אחרי שהמכירה הסתיימה";
            }
            var model = new modelOfItemAndCategory
            {
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                item_in_wait = db.item_in_wait.Where(e => e.idorder_wait == q.idorder_wait&&e.idclient_wait==q.idclient_wait&&e.iditem_wait==q.iditem_wait).ToList()
            };
            return View("DetailsWait",model);
        }
        //הוספת פריט להזמנה
        [HttpGet]
        public ActionResult AddItemOr(string data)
        {
           
            var ez = data.Substring(0, data.IndexOf(";"));
            int id_n = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.LastIndexOf(";") - 3);
            int ord = Convert.ToInt32(data.Substring(data.LastIndexOf(";") + 1, data.Length - ez.Length - size.Length - 2));
            var IdCustomer = Session["idC"];
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            var sale = db.sale.FirstOrDefault(i => i.idcity_sale == cli.idcity_client && i.idneighborhood_sale == cli.idneighborhood_client);          
            var d = db.order_details.FirstOrDefault(y => y.iditem_details == id_n && y.size_details.ToString() == size&&y.idorder_details==ord);
            if (d.orders.date_order <= sale.lod_sale && d.orders.date_order >= sale.fod_sale) { 
            var si = db.size.FirstOrDefault(p => p.item_in_sale.iditems == id_n && p.n_size.ToString() == size);
                if ((d.amount_details + 1) <= Convert.ToInt32(d.item.limitedamount_item) && (si.amount != 0 || si.amount - 1 >= 0))
                {
                    d.price += (float)d.item.price_item;
                    d.amount_details = d.amount_details + 1;
                    si.amount = si.amount - 1;
                }
                db.SaveChanges();
            }
            else
            {
                ViewBag.error = "אין אפשרות להזמין אחרי שהמכירה הסתיימה";
            }
            var model = new modelOfItemAndCategory
            {
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                order_details = db.order_details.Where(e => e.idorder_details == d.idorder_details&&e.iditem_details==d.iditem_details).ToList()
            };
            return View("DetailsOrders", model);
        }
        //הסרת פריט מרשימת המתנה
        [HttpGet]
        public ActionResult DeleteWa(string data)
        {

                var ez = data.Substring(0, data.IndexOf(";"));
                int id_n = Convert.ToInt16(ez);
                string size = data.Substring(data.IndexOf(";") + 1, data.LastIndexOf(";") - 3);
                DateTime date = DateTime.Parse(data.Substring(data.LastIndexOf(";") + 1, data.Length - ez.Length - size.Length - 2));
                var IdCustomer = Session["idC"];
                var cli = db.client.Find(Convert.ToInt32(IdCustomer));
                var sale = db.sale.FirstOrDefault(i => i.idcity_sale == cli.idcity_client && i.idneighborhood_sale == cli.idneighborhood_client);
                var d = db.item_in_wait.FirstOrDefault(y => y.iditem_wait == id_n && y.size_wait.ToString() == size && y.date_wait == date);
                if (d.orders.date_order <= sale.lod_sale && d.orders.date_order >= sale.fod_sale)
                {
                    var l = db.item.FirstOrDefault(p => p.id_item == id_n);
                    if (d.amount_wait >= 2)
                    {
                        d.amount_wait -= 1;
                        d.price -= (float)l.price_item;
                    }
                    else
                    {
                        db.item_in_wait.Remove(d);
                    }
                    db.SaveChanges();
                    var od = db.orders.FirstOrDefault(o => o.id_order == d.idorder_wait);
                    var or_d = db.item_in_wait.Where(i => i.idorder_wait == d.idorder_wait);

                    if (or_d.Count() == 0)
                    {
                        db.orders.Remove(od);
                        db.SaveChanges();
                    }
                }
                else
                {
                    ViewBag.error = "אין אפשרות לבטל הזמנת פריטים אחרי שהמכירה הסתיימה";
                }
            var orderW = db.item_in_wait.Where(c => c.idorder_wait == d.idorder_wait).ToList();
            if (orderW.Count() > 0)
            {
                var model = new modelOfItemAndCategory
                {
                    Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                    Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList(),
                    item_in_wait = db.item_in_wait.Where(e => e.idorder_wait == d.idorder_wait && e.idclient_wait == d.idclient_wait && e.iditem_wait == d.iditem_wait).ToList()
                };
                return View("DetailsWait", model);
            }
            return RedirectToAction("WaitOrders");

        }
        //הסרת פריט מרשימת הקניות
        [HttpGet]
        public ActionResult DeleteItem(string data)
        {
            var ez = data.Substring(0, data.IndexOf(";"));
            int id_n = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.Length-ez.Length - 1);
            var d = DBShopinCart.lShopingCard.FirstOrDefault(y => y.id_item == id_n&&y.sizeinstock_item==size);
            var l = db.size.FirstOrDefault(p => p.item_in_sale.id_itemsale == id_n && p.n_size.ToString() == size);
            var si = db.size.FirstOrDefault(t => t.item_in_sale.id_itemsale == id_n && t.n_size.ToString() == size);
            if (d.Amount >= 2)
            {
                d.Amount -= 1;
                d.price_item -= l.item_in_sale.item.price_item;
            }
            else
            {
                DBShopinCart.lShopingCard.Remove(d);
            }
            si.amount = si.amount + 1;
            db.SaveChanges();
            
            var IdCustomer = Session["idC"].ToString();
            delCookie(IdCustomer);
            SetCookie(DBShopinCart.lShopingCard, IdCustomer);
            return RedirectToAction("MyShoppingCart");
        }
        //הסרת כל הכמות של פריט מסל הקניות
        public ActionResult DeleteAllItem(string data)
        {
            var ez = data.Substring(0, data.IndexOf(";"));
            int id = Convert.ToInt16(ez);
            string size = data.Substring(data.IndexOf(";") + 1, data.Length-ez.Length - 1);
            var d = DBShopinCart.lShopingCard.FirstOrDefault(y => y.id_item == id && y.sizeinstock_item == size);
            var l = db.size.FirstOrDefault(p => p.item_in_sale.id_itemsale == id && p.n_size.ToString() == size);
            l.amount += d.Amount;
            db.SaveChanges();
            DBShopinCart.lShopingCard.Remove(d);
            
            var IdCustomer = Session["idC"].ToString();
            delCookie(IdCustomer);
            SetCookie(DBShopinCart.lShopingCard, IdCustomer);
            return RedirectToAction("MyShoppingCart");
        }
        //חיפוס פריט לפי שם 
        [HttpPost]
        public PartialViewResult Search(string searchTxt)
        {
            var IdCustomer = Session["idC"].ToString();
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            var model = new modelOfItemAndCategory
            {
                Items = db.item_in_sale.Where(i => i.item.name_item.Contains(searchTxt)&&i.sale.lod_sale>=DateTime.Now&&i.sale.idcity_sale == cli.idcity_client && i.sale.idneighborhood_sale == cli.idneighborhood_client).ToList(),
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList()
            };
            if (model.Items.Count() == 0)
            {
               ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך"; 
            }
                
                return PartialView(model);
        
        }
        //חיפוס פריט לפי טווח מחיר
        [HttpPost]
        public PartialViewResult SearchPrice(int SearchMinPrice, int SearchMaxPrice)
        {
            var IdCustomer = Session["idC"].ToString();
            var cli = db.client.Find(Convert.ToInt32(IdCustomer));
            var model = new modelOfItemAndCategory
            {
                Items = db.item_in_sale.Where(i => i.item.price_item > SearchMinPrice&& i.sale.lod_sale >= DateTime.Now && i.item.price_item < SearchMaxPrice && i.sale.idcity_sale == cli.idcity_client && i.sale.idneighborhood_sale == cli.idneighborhood_client ).ToList(),
                Category_itemsMan = db.category_item.Where(m => m.id_typeItem == 1 || m.id_typeItem == 5).ToList(),
                Category_itemsWoman = db.category_item.Where(m => m.id_typeItem == 2 || m.id_typeItem == 5).ToList()
            };

            if (model.Items.Count() == 0)
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
            } 
                return PartialView(model);
        }
      

        [HttpPost]
        public PartialViewResult SearchItem(string searchTxt)
        {
           
            var res = db.item_in_sale.Where(i => i.item.name_item.Contains(searchTxt)|| i.item.price_item.ToString().Contains(searchTxt) || i.item.category_item.name_category.Contains(searchTxt) || i.item.type_item.name_type.Contains(searchTxt)).ToList();
            if (res.Count() != 0)
            {
                return PartialView(res);
            }
            else
            {
                ViewBag.error9 = "אין תוצאות לשאילתה החיפוש שלך";
                return PartialView(res);
            }
        }

    }
}

