﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_type_itemController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_type_item
        public ActionResult Index()
        {
            return View(db.type_item.ToList());
        }


        // GET: Manager_type_item/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manager_type_item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken] 
        public ActionResult Create([Bind(Include = "id_type,name_type")] type_item type_item)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.type_item.Add(type_item);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(type_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_type_item/Edit/5
        public ActionResult Edit(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            type_item type_item = db.type_item.Find(id);
            if (type_item == null)
            {
                return HttpNotFound();
            }
            return View(type_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // POST: Manager_type_item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_type,name_type")] type_item type_item)
        {
            try{
                if (ModelState.IsValid)
            {
                db.Entry(type_item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(type_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: Manager_type_item/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                type_item type_item = db.type_item.Find(id);
                if (type_item == null)
                {
                    return HttpNotFound();
                }
                return View(type_item);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_type_item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                //מחיקת כל הנתונים המקושרים לסוג מוצר זה
                type_item type_item = db.type_item.Find(id);
                db.category_item.RemoveRange(db.category_item.Where(c => c.type_item.id_type == id));
                db.item_in_sale.RemoveRange(db.item_in_sale.Where(c => c.item.type_item.id_type == id));
                db.item_in_wait.RemoveRange(db.item_in_wait.Where(c => c.item.type_item.id_type == id));
                db.order_details.RemoveRange(db.order_details.Where(c => c.item.type_item.id_type == id));
                db.item.RemoveRange(db.item.Where(c => c.type_item.id_type == id));

                db.type_item.Remove(type_item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
