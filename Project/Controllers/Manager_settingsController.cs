﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    public class Manager_settingsController : Controller
    {
        private P2Entities2 db = new P2Entities2();

        // GET: Manager_settings
        public ActionResult Index()
        {
           /* Session["layoutname"] = Session["nameC"];*/
            return View(db.setting.ToList());
        }

     
        // GET: Manager_settings/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Manager_settings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_setting,minfamsize_setting,maxsalary_setting,numitems_setting")] setting setting)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.setting.Add(setting);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(setting);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שמירת הפרטים";
                return View();
            }
        }

        // GET: Manager_settings/Edit/5
        public ActionResult Edit(int? id)
        {
           try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            setting setting = db.setting.Find(id);
            if (setting == null)
            {
                return HttpNotFound();
            }
            return View(setting);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }


        // POST: Manager_settings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_setting,minfamsize_setting,maxsalary_setting,numitems_setting")] setting setting)
        {
            try{
                if (ModelState.IsValid)
            {
                db.Entry(setting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(setting);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת עדכון הפרטים";
                return View();
            }
        }

        // GET: Manager_settings/Delete/5
        public ActionResult Delete(int? id)
        {
            try{
                if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            setting setting = db.setting.Find(id);
            if (setting == null)
            {
                return HttpNotFound();
            }
            return View(setting);
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת שליחת הנתונים";
                return View();
            }
        }

        // POST: Manager_settings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           try{
                setting setting = db.setting.Find(id);
            db.setting.Remove(setting);
            db.SaveChanges();
            return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.error = ".ארעה שגיאה בעת מחיקת הנתונים";
                return View();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
