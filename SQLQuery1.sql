﻿ALTER TABLE dbo.client ADD nameclient VARCHAR(30)
ALTER TABLE dbo.client ADD numchildren_client INT
ALTER TABLE dbo.client ADD lastorder_client DATE
ALTER TABLE dbo.orders ADD idorder_detail INT
ALTER TABLE dbo.sale ADD numitemssetting_sale INT 
CONSTRAINT fsetting foreign  key (numitemssetting_sale) references setting (id_setting)
ALTER TABLE dbo.sale ADD 
CONSTRAINT fneighborhoodsale foreign  key (idneighborhood_sale) references neighborhood(id_neighborhood)
ALTER TABLE dbo.orders ADD CONSTRAINT forderdetails
 foreign  key (idorder_detail) references order_details (idorder_details)